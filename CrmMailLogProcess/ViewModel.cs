﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrmMailLogProcess
{
    class GenericResultVm
    {
        public string result { get; set; }
    }

    class MemberListVm
    {
        public string CompanyName { get; set; }

        public string ID { get; set; }

        public string Table { get; set; }
    }

    class MessageCategoryVm
    {
        public string NoteCategoryId { get; set; }

        public string ValueName { get; set; }
    }
}
