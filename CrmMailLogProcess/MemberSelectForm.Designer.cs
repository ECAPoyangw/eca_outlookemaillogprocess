﻿namespace CrmMailLogProcess
{
    partial class MemberSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemberSelectForm));
            this.lstMemberList = new System.Windows.Forms.ListBox();
            this.txbMemberFilter = new System.Windows.Forms.TextBox();
            this.lblMemberFilter = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.submit = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.lstMemberSelect = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbMessageCategory = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lstMemberList
            // 
            this.lstMemberList.FormattingEnabled = true;
            this.lstMemberList.Location = new System.Drawing.Point(15, 249);
            this.lstMemberList.Name = "lstMemberList";
            this.lstMemberList.Size = new System.Drawing.Size(354, 95);
            this.lstMemberList.Sorted = true;
            this.lstMemberList.TabIndex = 0;
            this.lstMemberList.DoubleClick += new System.EventHandler(this.lstMemberList_DoubleClick);
            // 
            // txbMemberFilter
            // 
            this.txbMemberFilter.Location = new System.Drawing.Point(15, 187);
            this.txbMemberFilter.Name = "txbMemberFilter";
            this.txbMemberFilter.Size = new System.Drawing.Size(354, 20);
            this.txbMemberFilter.TabIndex = 1;
            this.txbMemberFilter.TextChanged += new System.EventHandler(this.txbMemberFilter_TextChanged);
            // 
            // lblMemberFilter
            // 
            this.lblMemberFilter.AutoSize = true;
            this.lblMemberFilter.Location = new System.Drawing.Point(16, 168);
            this.lblMemberFilter.Name = "lblMemberFilter";
            this.lblMemberFilter.Size = new System.Drawing.Size(165, 13);
            this.lblMemberFilter.TabIndex = 2;
            this.lblMemberFilter.Text = "Filter the Members by search term";
            // 
            // label2
            // 
            this.label2.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "This e-mail is associated with (double-click to remove):";
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(115, 356);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(75, 23);
            this.submit.TabIndex = 4;
            this.submit.Text = "OK";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(196, 356);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 5;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // lstMemberSelect
            // 
            this.lstMemberSelect.BackColor = System.Drawing.SystemColors.Control;
            this.lstMemberSelect.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstMemberSelect.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.lstMemberSelect.FormattingEnabled = true;
            this.lstMemberSelect.IntegralHeight = false;
            this.lstMemberSelect.Location = new System.Drawing.Point(15, 78);
            this.lstMemberSelect.Name = "lstMemberSelect";
            this.lstMemberSelect.Size = new System.Drawing.Size(354, 13);
            this.lstMemberSelect.TabIndex = 6;
            this.lstMemberSelect.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstMemberSelect_DrawItem);
            this.lstMemberSelect.DoubleClick += new System.EventHandler(this.lstMemberSelect_DoubleClick);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(357, 51);
            this.label3.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "(Double-click to select)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Select a message category (required)";
            // 
            // cmbMessageCategory
            // 
            this.cmbMessageCategory.FormattingEnabled = true;
            this.cmbMessageCategory.Location = new System.Drawing.Point(15, 127);
            this.cmbMessageCategory.Name = "cmbMessageCategory";
            this.cmbMessageCategory.Size = new System.Drawing.Size(354, 21);
            this.cmbMessageCategory.TabIndex = 10;
            // 
            // MemberSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(383, 390);
            this.Controls.Add(this.cmbMessageCategory);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstMemberSelect);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblMemberFilter);
            this.Controls.Add(this.txbMemberFilter);
            this.Controls.Add(this.lstMemberList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MemberSelectForm";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 13, 13);
            this.Text = "Tag additional companies";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstMemberList;
        private System.Windows.Forms.TextBox txbMemberFilter;
        private System.Windows.Forms.Label lblMemberFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.ListBox lstMemberSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbMessageCategory;
    }
}