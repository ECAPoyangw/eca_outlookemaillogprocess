﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;


namespace CrmMailLogProcess
{
    class MailHandler
    {

        const string PR_SMTP_ADDRESS = "http://schemas.microsoft.com/mapi/proptag/0x39FE001E";
        const string PR_ATTACH_CONTENT_ID = "http://schemas.microsoft.com/mapi/proptag/0x3712001E";
        const string PR_INTERNET_MESSAGE_ID_W_TAG = "http://schemas.microsoft.com/mapi/proptag/0x1035001F";
        string ServerRoot = ConfigurationManager.AppSettings["ServerRoot"]; // @"\\UATWB03VM\CRMEmail\";
        string Domain = ConfigurationManager.AppSettings["Domain"]; //@"http://ecacrm.uat.eca.domain/";        
        string dtUploadDate = DateTime.Now.ToString("yyyy-MM-dd HH':'mm':'ss");
        int failedAttempts = 0;
        int maxAttempts = 0; // Leave this as 0 for now, so app does NOT attempt to rollback after failing
        string username = WindowsIdentity.GetCurrent().Name;

        //Outlook.Application oApp = new Outlook.Application();
        Outlook.Application oApp = (Outlook.Application)Globals.ThisAddIn.Application;
        Outlook.MailItem oMsg;


        // This method is called when the UPLOAD button is pressed.


        public void DataCollector(string members, string messageCategory)
        {

            ErrorLogger.Log("Data Collector Method Called");
            if (members == null)
            {
                members = "";
            }

            if (messageCategory == null)
            {
                messageCategory = "";
            }

            string mMessageId = GetMessageId();
            string mRecipients = GetRecipients();
            string mSender = GetSender();
            string mSubject = GetSubject();
            string mSentDate = GetSentDate();
            string mAttachments = GetAttachments();
            //string mFilePath = GetFilePath();
            string mUploader = username;
            string mUploadDate = dtUploadDate;
            string mMemberTags = members;
            string mMessageCategory = messageCategory;
            string mCC = GetRecipientsCC();
            string mBCC = GetRecipientsBCC();
            string result = null;



            // Upload the data to the database

            result = DbUpload(mMessageId, mRecipients, mSender, mSubject, mSentDate, mAttachments, /*mFilePath,*/ mUploader, mUploadDate, mMemberTags, mMessageCategory, mCC, mBCC);
            ErrorLogger.Log("DpUpload( " + mMessageId + "," + mRecipients + "," + mSender + "," + mSubject + "," + mSentDate + "," + mAttachments + "," + /*mFilePath + "," +*/ mUploader + "," + mUploadDate + "," + mMemberTags + "," + mMessageCategory + "," + mCC + "," + mBCC + ") called with result: " + result);
            if (result != null)
            {
                FileUpload(result, mMessageId, mUploadDate);
            }


        }


        private string DbUpload(string messageId, string recipients, string senderEmail, string subject, string sentDate, string attachments, /*string filepath,*/ string uploader, string uploadDate, string memberTags, string messageCategory, string recipientsCC, string recipientsBCC, int appMode = 0)
        {
            // Show the "waiting" cursor to the user...

            ErrorLogger.Log("DpUpload method of MailHandler called.");
            Cursor.Current = Cursors.WaitCursor;


            DbConnection conn = new DbConnection();

            string sqlCmd = "procMem_CrmMailSave";

            var sqlParams = new object[] {
                new { name = "@strMessageId", value = messageId },
                new { name = "@strRecipients", value = recipients },
                new { name = "@strSenderEmail", value = senderEmail },
                new { name = "@strSubject", value = subject },
                new { name = "@strSentDate", value = sentDate },
                new { name = "@strAttachments", value = attachments },
                //new { name = "@strFilePath", value = filepath },
                new { name = "@strUploader", value = uploader },
                new { name = "@strUploadDate", value = uploadDate },
                new { name = "@strMemberTags", value = memberTags },
                new { name = "@strNoteCategory", value = messageCategory },
                new { name = "@strRecipientsCC", value = recipientsCC },
                new { name = "@strRecipientsBCC", value = recipientsBCC },
                new { name = "@iAppMode", value = appMode }
            };

            var sqlresult = conn.SqlSpResult<GenericResultVm>(sqlCmd, sqlParams);

            // If sql returns a NULL result then close any open forms and pass Null to the calling method
            if (sqlresult == null)
            {
                FormCollection forms = Application.OpenForms;

                if (forms != null && forms.Count > 0)
                {
                    for (var i = 0; i < forms.Count; i++)
                    {
                        forms[i].Close();
                    }

                }
                sqlresult = new List<GenericResultVm>();
                sqlresult.Add(new GenericResultVm() { result = "Upload Failed" });
            }

            ErrorLogger.Log("DpUpload method returns: " + sqlresult[0].result.ToString());
            return sqlresult[0].result.ToString();

        }
        private string UploadFileToCloud(string mailID, string msg)
        {
            DbConnectionToCorDocument con = new DbConnectionToCorDocument();
            string sqlCmd = "procGMem_MemberEmail_INSERT_UPDATE";
            var sqlParams = new object[] {
                new { name = "@MemberOutlookEmailId", value = mailID },
                new { name = "@EmailBase64", value = msg }

            };


            var sqlresult = con.SqlSpResult(sqlCmd, sqlParams);
            if (sqlresult == null)
            {
                FormCollection forms = Application.OpenForms;

                if (forms != null && forms.Count > 0)
                {
                    for (var i = 0; i < forms.Count; i++)
                    {
                        forms[i].Close();
                    }

                }
                //sqlresult = new List<GenericResultVm>();
                //sqlresult.Add(new GenericResultVm() { result = "Upload Failed" });
            }
            //ErrorLogger.Log("DpUpload method returns: " + sqlresult[0].result.ToString());
            return sqlresult.ToString();
        }

        private void FileUpload(string result, string messageId, string uploadDate)
        {
            // An example of result text...
            //result = "Upload Success [folder:340994-34324-34234-343] [folder:23984-4341645-26982] [filename:934893843-233543-265]";
            ErrorLogger.Log("FileUpload method of MailHandler called.");
            if (result.Contains("Upload Success"))
            {

                string param1 = "folder:";
                string param2 = "filename:";
                string param3 = "member:";
                string param4 = "memberOutlookEmailId";
                string fileExtn = ".msg";
                string file = "";
                string folder = "";
                string member = "";
                string memberOutlookEmailId = "";
                int startPos = 0;

                // First check if ServerRoot exists, create it if not. If ServerRoot cannot be created then remove the recently added records and exit
                try
                {
                    if (Directory.Exists(ServerRoot) == false)
                    {
                        Directory.CreateDirectory(ServerRoot);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(ex.Message.ToString());
                    MessageBox.Show("The process failed: " + ex.Message.ToString());
                    // Run the procedure again in rollback mode then exit
                    var rollback = DbUpload(messageId, "", "", "", "", "", /*null,*/ "", uploadDate, "", "", "", "", 1);
                    return;
                }


                // If the result string contains the proposed filename (CompanyActivityId) then split it from the rest of the string
                // otherwise remove the recently added records and exit
                if (result.Contains(param2))
                {
                    startPos = result.IndexOf(param2) + (param2.Length);
                    file = result.Replace(result.Substring(0, startPos), ""); // Clear string upto this point
                    file = file.Substring(0, file.IndexOf("]") + 1); // Make substring upto IndexOf("]")
                    file = file.Replace("[", ""); // Get rid of extra characters
                    file = file.Replace("]", ""); // Get rid of extra characters
                }
                else
                {
                    // Run the procedure again in rollback mode then exit
                    var rollback = DbUpload(messageId, "", "", "", "", "", /*null,*/ "", uploadDate, "", "", "", "", 1);
                    return;
                }


                // Now that ServerRoot exists try to save message file in each company's mail folder. Otherwise remove the recently added records and exit
                while (result.Contains(param1))
                {
                    startPos = result.IndexOf(param1) + (param1.Length);
                    folder = result.Replace(result.Substring(0, startPos), ""); // Clear string upto this point
                    folder = folder.Substring(0, folder.IndexOf("]") + 1); // Make substring upto IndexOf("]")
                    folder = folder.Replace("[", ""); // Get rid of extra characters
                    folder = folder.Replace("]", ""); // Get rid of extra characters

                    try
                    {
                        //string filepath = ServerRoot + folder;

                        string filepath = Path.GetTempPath() + folder;
                        // Check if folder exists, create it if not
                        if (Directory.Exists(filepath) == false)
                        {
                            Directory.CreateDirectory(filepath);
                        }

                        // Save the file to the specified folder
                        oMsg.SaveAs(filepath + @"\" + file + fileExtn);

                        //convert msg file to msgbase64 type
                        if (result.Contains(param4))
                        {
                            startPos = result.IndexOf(param4) + (param4.Length);
                            memberOutlookEmailId = result.Replace(result.Substring(0, startPos), ""); // Clear string upto this point
                            memberOutlookEmailId = memberOutlookEmailId.Substring(0, memberOutlookEmailId.IndexOf("]") + 1); // Make substring upto IndexOf("]")
                            memberOutlookEmailId = memberOutlookEmailId.Replace("[", ""); // Get rid of extra characters
                            memberOutlookEmailId = memberOutlookEmailId.Replace("]", ""); // Get rid of extra characters
                            memberOutlookEmailId = memberOutlookEmailId.Replace(":", ""); // Get rid of extra characters
                        }
                        else
                        {
                            // Run the procedure again in rollback mode then exit
                            var rollback = DbUpload(messageId, "", "", "", "", "", /*null,*/ "", uploadDate, "", "", "", "", 1);
                            return;
                        }

                        var msgByte = File.ReadAllBytes(filepath + @"\" + file + fileExtn);
                        var msgBase64 = Convert.ToBase64String(msgByte);

                        var a = UploadFileToCloud(memberOutlookEmailId, msgBase64);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("ErrorMsg: " + ex.Message + "\n" +
                                        "ErrorStackTrace: " + ex.StackTrace);

                        MessageBox.Show("The process failed: " + ex.Message.ToString());
                        // Run the procedure again in rollback mode then exit
                        var rollback = DbUpload(messageId, "", "", "", "", "", /*null,*/ "", uploadDate, "", "", "", "", 1);
                        return;
                    }

                    // Strip folder name from result string
                    result = result.Replace(result.Substring(0, result.IndexOf("]") + 1), "");
                    //Debug: MessageBox.Show(result);
                }
                MessageBox.Show("Successfully uploaded to ECA Connect");


                if (result.Contains(param3))
                {
                    startPos = result.IndexOf(param3) + (param3.Length);
                    member = result.Replace(result.Substring(0, startPos), ""); // Clear string upto this point
                    member = member.Substring(0, member.IndexOf("]") + 1); // Make substring upto IndexOf("]")
                    member = member.Replace("[", ""); // Get rid of extra characters
                    member = member.Replace("]", ""); // Get rid of extra characters
                }
                //http://localhost:61324/Member/LogOutlookProcess?memberId=1790cc9f-9ec0-4f95-aacf-a68e00ad49fd
                // for uat path: http://ecacrm.uat.eca.local/Member/LogOutlookProcess?memberId=1790cc9f-9ec0-4f95-aacf-a68e00ad49fd
                // for live path: http://connect.eca.local/Member/LogOutlookProcess?memberId=
                // for live path: https://ecaconnect.eca.co.uk/Member/LogOutlookProcess?memberId=
                System.Diagnostics.Process.Start("https://ecaconnect.eca.co.uk/Member/LogOutlookProcess?memberId=" + member);   //1790cc9f - 9ec0-4f95-aacf-a68e00ad49fd");

            }
            // If some of the recipients' e-mail addresses are not recognised in ECA CRM then request for additional members to be tagged
            else if (result.Contains("Upload Tentative"))
            {
                string displayMessage = result.Replace("Upload Tentative. ", "");
                Form MemberSelectFrm = new MemberSelectForm(displayMessage);
                MemberSelectFrm.Show();
            }
            else if (result.Contains("Upload Missing Category"))
            {
                string displayMessage = result.Replace("Upload Missing Category. ", "");
                string taggedMemberIds = displayMessage.Substring(displayMessage.IndexOf("["));

                displayMessage = displayMessage.Replace(taggedMemberIds, "");


                taggedMemberIds = taggedMemberIds.Replace("[", "").Replace("]", "");
                string[] tags = new string[] { taggedMemberIds };

                Form MemberSelectFrm = new MemberSelectForm(displayMessage, tags);
                MemberSelectFrm.Show();
            }
            else if (result.Contains("Upload Failed"))
            {

                if (ErrorLogger.goodConnection == true)
                {
                    var rollback = DbUpload(messageId, "", "", "", "", "", /*null,*/ "", uploadDate, "", "", "", "", 1);

                    if (rollback.Contains("Database clean-up has completed successfully"))
                    {
                        return;
                    }
                }
                return;

            }
            else
            {
                // The method failed so let the user know
                MessageBox.Show(result);
            }



        }

        // Get the Message Id from the headers of the message
        private string GetMessageId()
        {
            string mMessageId = "";

            mMessageId = oMsg.PropertyAccessor.GetProperty(PR_INTERNET_MESSAGE_ID_W_TAG);

            // Get rid of unneccessary characters from mMessageId string
            mMessageId = mMessageId.Replace("<", "");
            mMessageId = mMessageId.Replace(">", "");

            // If message does not contain a MessageId (perhaps because Outlook does not record MessageId of sent items in Cached mode)...
            // create a custom user property called CustomMessageId for this e-mail
            if (mMessageId == "")
            {
                // if CustomMessageId property does not yet exist then create it and add to the message
                var customMessageId = oMsg.UserProperties.Find("CustomMessageId");
                if (customMessageId == null)
                {

                    customMessageId = oMsg.UserProperties.Add("CustomMessageId", Outlook.OlUserPropertyType.olText, false, 1);
                    customMessageId.Value = Guid.NewGuid().ToString();
                    oMsg.Save();
                    ErrorLogger.Log("CustomMessageId created: +");
                }

                mMessageId = customMessageId.Value;

            }
            ErrorLogger.Log("mMessageId = " + mMessageId);
            return mMessageId;
        }

        // Get the e-mail addresses of recipients
        private string GetRecipients()
        {
            string mRecipients = "";
            string separator = "";
            int recipCount = 0;

            // Build a comma-separated string containing the e-mail addresses of each recipient
            Outlook.Recipients oRecips = oMsg.Recipients;

            foreach (Outlook.Recipient recip in oRecips)
            {
                recipCount++;
                separator = recipCount == oMsg.Recipients.Count ? "" : ",";
                mRecipients += recip.PropertyAccessor.GetProperty(PR_SMTP_ADDRESS).ToString() + separator;
            }

            // Handle apostrophe in email address
            mRecipients = mRecipients.Replace("'", "''");

            ErrorLogger.Log("mReceipients = " + mRecipients);
            return mRecipients;
        }

        // Get a string of recipients' email addresses who were cc'd
        private string GetRecipientsCC()
        {
            string mRecipientsCC = "";
            string separator = "";
            int recipCount = 0;

            Outlook.Recipients oRecips = oMsg.Recipients;

            foreach (Outlook.Recipient recipCC in oRecips)
            {
                recipCount++;
                separator = recipCount == oMsg.Recipients.Count ? "" : ",";

                if (recipCC.Type == (int)Outlook.OlMailRecipientType.olCC)
                {
                    mRecipientsCC += recipCC.PropertyAccessor.GetProperty(PR_SMTP_ADDRESS).ToString() + separator;
                }
            }

            // Handle apostrophe in email address
            mRecipientsCC = mRecipientsCC.Replace("'", "''");
            ErrorLogger.Log("mReceipientsCC = " + mRecipientsCC);
            return mRecipientsCC;
        }

        // Get a string of recipients' email addresses who were bcc'd
        private string GetRecipientsBCC()
        {
            string mRecipientsBCC = "";
            string separator = "";
            int recipCount = 0;

            Outlook.Recipients oRecips = oMsg.Recipients;

            foreach (Outlook.Recipient recipBCC in oRecips)
            {
                recipCount++;
                separator = recipCount == oMsg.Recipients.Count ? "" : ",";

                if (recipBCC.Type == (int)Outlook.OlMailRecipientType.olBCC)
                {
                    mRecipientsBCC += recipBCC.PropertyAccessor.GetProperty(PR_SMTP_ADDRESS).ToString() + separator;
                }
            }

            // Handle apostrophe in email address
            mRecipientsBCC = mRecipientsBCC.Replace("'", "''");
            ErrorLogger.Log("mReceipientsBCC = " + mRecipientsBCC);
            return mRecipientsBCC;
        }


        // Get the sender's e-mail address. Resolve Exchange User if necessary
        private string GetSender()
        {
            string mSender = "";

            string currentUserEmail = oApp.Application.ActiveExplorer().Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
            //   ErrorLogger.Log("currentUserEmail: " + currentUserEmail);
            ErrorLogger.Log("Inside GetSender Method");
            //  ErrorLogger.Log("Testing method SendUsingAccount.SmtpAddress " + oMsg.SendUsingAccount.SmtpAddress);

            if (oMsg.SenderEmailType != null)
            {
                ErrorLogger.Log("Sender type: " + oMsg.SenderEmailType);


                // Get the e-mail address of the sender. If sender is exchange user resolve the email address from the name, otherwise use the email address string
                if (oMsg.SenderEmailType == "EX")
                {
                    // ErrorLogger.Log("About to call <mSender = ResolveSmtpAddress(oMsg.SenderName.ToString())>");
                    mSender = ResolveSmtpAddress(oMsg.SenderName.ToString());
                }
                else
                {

                    mSender = oMsg.SenderEmailAddress;
                }

                mSender = mSender.Replace("'", "''");

            }

            else
            {
                if (currentUserEmail != null)
                {

                    mSender = currentUserEmail;
                    mSender = mSender.Replace("'", "''");
                }

            }



            // Handle apostrophe in email address

            ErrorLogger.Log("Returning mSender = " + mSender);
            return mSender;
        }



        // Get the date/time the message was sent
        private string GetSentDate()
        {
            string mSentDate = "";

            if (oMsg.SentOn != null)
            {
                mSentDate = oMsg.SentOn.ToString("yyyy-MM-dd HH':'mm':'ss");

            }
            ErrorLogger.Log("mSentDate = " + mSentDate);
            return mSentDate;
        }


        // Get the subject of the e-mail
        private string GetSubject()
        {
            string mSubject = "";

            if (oMsg.Subject != null)
            {
                mSubject = oMsg.Subject.ToString();
                mSubject = mSubject.Replace("'", "''");
            }

            // Handle apostrophe in subject

            ErrorLogger.Log("mSubject = " + mSubject);
            return mSubject;
        }


        // Get the server root and convert to file path on web server
        // *** This was my original method... ***
        // private string GetFilePath()
        //{
        //    string mFilePath = "";
        //    string serverDir = "";

        //    Uri uri = new Uri(ServerRoot);

        //    string[] uriSegments = uri.Segments;
        //    // Build a string containing all of the segments of the URI instead of the first (host)
        //    for (int i = 1; i < uriSegments.Length; i++)
        //    {
        //        // Exclude drive letter
        //        if (uriSegments[i].Contains("$") == false)
        //        {
        //            serverDir += uriSegments[i];
        //        }
        //    }

        //    mFilePath = Domain + serverDir;

        //    // Add backslash character to the end of the string if it is not already there
        //    mFilePath = mFilePath.Substring(mFilePath.Length - 1) == "/" ? mFilePath : mFilePath + "/";

        //    return mFilePath;
        //}

        //private string GetFilePath(string memberNumber = "420106")
        //{
        //    string filePath = "";
        //    string memberFolder = "";
        //    int iMemberNumber = Int32.Parse(memberNumber);

        //    if (iMemberNumber > 1)
        //    {
        //        int top = (int)Math.Ceiling((decimal)iMemberNumber / 10000);
        //        //int top = (int)(Math.Ceiling((decimal)iMemberNumber / 10000) * 10000);
        //        int bottom = top - 10000;
        //        memberFolder = string.Format("{0}-{1}", bottom, top);

        //        filePath = memberFolder;
        //    }
        //    return filePath;

        //}

        private string GetMessageCategory()
        {
            string mMessageCategory = "";

            return mMessageCategory;
        }

        // Create a string comprised of the filenames of the attachments, excluding files in the body of the message i.e. signature logos
        private string GetAttachments()
        {
            string mAttachments = "";
            int rawAttachments = oMsg.Attachments.Count;

            if (rawAttachments > 0)
            {
                Outlook.Attachments attachments = oMsg.Attachments;

                foreach (Outlook.Attachment oAtt in attachments)
                {
                    // Check if the attachment is embedded in message body. If not then append filename to mAttachments
                    if (oAtt.PropertyAccessor.GetProperty(PR_ATTACH_CONTENT_ID) == "")
                    {
                        mAttachments += oAtt.FileName + ", ";
                    }
                }

                // Strip the ", " character from the end of the string
                if (mAttachments.Length > 0 && mAttachments[mAttachments.Length - 2] == ',') // minus 2 because of comma and [space]
                {
                    mAttachments = mAttachments.Substring(0, mAttachments.Length - 2);
                }
            }

            // Handle apostrophe in file names
            mAttachments = mAttachments.Replace("'", "''");

            ErrorLogger.Log("mAttachments = " + mAttachments);
            return mAttachments;
        }


        // Resolve the SMTP address of Exchange User by name
        public string ResolveSmtpAddress(string senderName)
        {
            // todo: This method needs to be changed to handle multiple instances of the same name
            // note: exchange users have unique names so this should be fine
            ErrorLogger.Log("ResolveSmtpAddress method, senderName passed: " + senderName);
            string senderSmtp = "";
            try
            {
                Outlook.Recipient oRecip = oApp.Session.CreateRecipient(senderName);
                oRecip.Resolve();

                senderSmtp = oRecip.AddressEntry.GetExchangeUser().PropertyAccessor.GetProperty(PR_SMTP_ADDRESS);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Error ex +" + ex.Message.ToString() + " StakeTrace: " + ex.StackTrace.ToString());
                senderSmtp = senderName;
            }

            ErrorLogger.Log("sender ResolvedSmtp = " + senderSmtp);
            return senderSmtp;
        }

        public void MemberSelectForm(string message)
        {
            MemberSelectForm OpenMemberSelect = new MemberSelectForm(message);
        }


        public MailHandler()
        {
            // Work with the current open message
            oMsg = oApp.ActiveInspector().CurrentItem;

        }
    }

}
